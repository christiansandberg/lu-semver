#!/usr/bin/python3

import sys
sys.dont_write_bytecode = True

from src.__main__ import main

if __name__ == '__main__':
    main()
